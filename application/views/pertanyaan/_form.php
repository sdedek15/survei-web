 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div>';
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Pertanyaan</h3>
		<hr />	 
			<table class="table table-bordered table-striped" id="myTable" width=100%>
                <thead>
                            <tr>
                                <th>No</th>
                                <th>Aksi</th>
                                <th>Id Survei</th>
                                <th>Nama Pertanyaan</th>
                                <th>Jenis Jawaban</th>
                                <th>Jawaban A</th>
                                <th>Jawaban B</th>
                                <th>Jawaban C</th>
                                <th>Jawaban D</th>
                                <th>Wajib Isi</th>
                                <th>Created At</th>
                                <!-- <th>Update At</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i=1;
                            foreach ($pertanyaan as $prt => $p){
                                error_reporting(0);
                            ?>
                            <tr>
                               
                                <td><?php echo $i; ?></td>
                                <td>
                                <a href="<?php echo base_url()?>Pertanyaan/update/<?php echo $p['id']?>" class="btn btn-warning"><i class = "fa fa-edit"></i></a>&nbsp;
                                <a href="<?php echo base_url()?>Pertanyaan/delete/<?php echo $p['id']?>" class="btn btn-danger"><i class = "fa fa-trash"></i></a>&nbsp;
                                </td>
                                <td><?php echo $p['id_survei']; ?></td>
                                <td><?php echo $p['nama_pertanyaan']; ?></td>
                                <td><?php echo $p['jenis_jawaban']; ?></td>
                                <td>
                                    <?php if ($p['jawaban_a'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_a'];
                                       } ?>
                                </td>
                                <td>
                                    <?php if ($p['jawaban_b'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_b'];
                                       } ?>
                                </td>
                                <td>
                                    <?php if ($p['jawaban_c'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_c'];
                                       } ?>
                                </td>
                                <td>
                                    <?php if ($p['jawaban_d'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_d'];
                                       } ?>
                                </td>
                                <td><?php echo $p['wajib_isi']; ?></td> 
                                <td><?php echo $p['created_at']; ?></td> 
                                <!-- <td><?php echo $p['update_at']; ?></td>  -->
                                <!-- <td><a href=""></a></td>  -->
                            </tr>
                            <?php $i++; }?>
                        </tbody>
				</table>    


		<?php echo form_open_multipart($action);?>
		
		<div class="form-group">
				<?php 
					// echo form_label('Id Survei');
					echo form_error('id_survei');
					echo form_input($id_survei);
				?>				
			</div>	
		<div class="form-group">
				<?php 
					
					echo form_label('Nama Survei');
					echo form_input($nama_survei);
					// echo form_input($nama_survei,$nama_survei);
				?>				
			</div>	
	    <div class="form-group">
				<?php 
					echo form_label('Nama Pertanyaan');
					echo form_error('nama_pertanyaan');
					echo form_input($nama_pertanyaan);
				?>				
			</div>
		<div class="form-group">
				<?php
					echo form_label('Jenis Jawaban');
					echo form_error('jenis_jawaban');
					$jawaban = array(
						'' => '',
						'Radio' => 'Radio',
						'Text' => 'Text',
						'Checkbox' => 'Checkbox',
						'Date' => 'Date',
					);
					echo form_dropdown($jenis_jawaban,$jawaban,$jenis_jawaban['value']);
				?>				
			</div>
		<div class="field_wrapper">
			<div class="form-group">
				<label>Jawaban</label><br>
				<input type="text" name="jawaban[]" value=""/>
				<a href="javascript:void(0);" class="btn btn-primary add_button" title="Add field"><i class = "fa fa-plus"></i></a>
			</div>
		</div>	
		
        <div class="form-group">
				<?php 
					echo form_error('wajib_isi');
					if ($wajib_isi['value']=='Ya'){
					echo form_checkbox('wajib_isi', 'Ya', true);
					}else{
					echo form_checkbox('wajib_isi', 'Ya');
					}
					echo form_label('Wajib Isi');
				?>	
				<br>
			</div>
	    <?php 
			echo form_input($id);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('Survei','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
	<br>
	<p>NB: Untuk Jenis Jawaban Text dan Date tidak perlu input jawaban, biarkan saja kosong !!</p>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 15; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="form-group"><input type="text" name="jawaban[]" value=""/> <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

    
