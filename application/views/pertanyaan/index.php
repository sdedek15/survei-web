  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
        <?php echo lang('member_heading');?>
        <small><?php echo lang('member_subheading');?></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
  <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"></h3><hr />	
			<div class="box-tools pull-right">
				<!-- <?php echo anchor('auth/create_user', lang('index_create_user_link'),array('class'=>'btn btn-flat btn-primary'))?>  <?php echo anchor('auth/create_group', lang('index_create_group_link'),array('class'=>'btn btn-flat btn-default'))?> -->
            </div>
		</div>
            <div class="box-body" >
				<table class="table table-bordered table-striped" id="myTable" width=100%>
                <thead>
                            <tr>
                                <th>No</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <!-- <th>Active</th> -->
                                <!-- <th>Status Mining</th> -->
                                <th>Investasi</th>
                                <th>Profit</th>
                                <th>Credit</th>
                                <th>Debit</th>
                                <th>Image</th>
                                <th>Status Mining</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i=1;
                            foreach ($users as $user => $usr){
                                error_reporting(0);

                                $status = (int)$usr['minningStatus'];
                            ?>
                            <tr>
                               
                                <td><?php echo $i; ?></td>
                                <td><?php echo $usr['username']; ?></td>
                                <td><?php echo $usr['email']; ?></td>
                                <td><?php echo $usr['gender']; ?></td>
                                <!-- <td><?php 
                                    if ($usr['active'] = false){
                                        echo 'Aktif';
                                    }else{
                                        echo 'Tidak Aktif';
                                    } ?></td> -->
                                    <!-- <td><?php 
                                    if ($usr['minningStatus'] = 0){
                                        echo 'Tidak Aktif';
                                    }else{
                                        echo 'Aktif';
                                    } ?></td> -->
                                <!-- <td><?php echo $usr['minningStatus']; ?></td> -->
                                <td><?php echo $usr['totalInvestasi']; ?></td>
                                <td><?php echo $usr['totalProfit']; ?></td> 
                                <td><?php echo $usr['creditAmount']; ?></td> 
                                <td><?php echo $usr['debitAmount']; ?></td> 
                                <td><img src="<?php echo $usr['imageURL'];?>" height="90" alt="" /></td> 
                                <td>
                                  <?php if ($status == 1) {
                                      echo '<a class="btn btn-info"> <b>Aktif</a>';
                                  }else {
                                      echo '<a class="btn btn-danger"> Non Akitf </a>';
                                  } ?>
                                </td> 
                                <td>
                                <a href="<?php echo base_url()?>Member/updatemember/<?php echo $usr['id']?>" class="btn btn-info">Edit</a>&nbsp;
                                <a href="<?php echo base_url()?>TransaksiDepo/getList/<?php echo $usr['id']?>" class="btn btn-primary">History Depo</a>&nbsp;
                                <a href="<?php echo base_url()?>TransaksiWD/getList/<?php echo $usr['id']?>" class="btn btn-primary">History WD</a>&nbsp;
                                </td>
                            </tr>
                            <?php $i++; }?>
                        </tbody>
				</table>        
			</div>       
    </section>
   <!-- /.content -->
  </div>
<script src="<?php echo base_url('resources/js/jquery-1.11.2.min.js') ?>"></script>  
<script>
  $(document).ready( function () {
 		$('#myTable').DataTable({
        "scrollX": true
    });
  } );
</script>