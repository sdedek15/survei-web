  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
      <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
  <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title">Daftar Pertanyaan</h3><hr />	
			<div class="box-tools pull-right">
				<!-- <?php echo anchor(site_url('Pertanyaan/create'), '<i class = "fa fa-plus"></i> Tambah Data', 'class="btn btn-flat btn-info"'); ?> -->
            </div>
		</div>
            <div class="box-body" >
				<table class="table table-bordered table-striped" id="myTable" width=100%>
                <thead>
                            <tr>
                                <th>No</th>
                                <th>Aksi</th>
                                <th>Id Survei</th>
                                <th>Nama Pertanyaan</th>
                                <th>Jenis Jawaban</th>
                                <th>Jawaban A</th>
                                <th>Jawaban B</th>
                                <th>Jawaban C</th>
                                <th>Jawaban D</th>
                                <th>Created At</th>
                                <th>Update At</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i=1;
                            foreach ($pertanyaan as $prt => $p){
                                error_reporting(0);
                            ?>
                            <tr>
                               
                                <td><?php echo $i; ?></td>
                                <td><a href="<?php echo base_url()?>Pertanyaan/delete/<?php echo $p['id']?>" class="btn btn-danger"><i class = "fa fa-trash"></i></a>&nbsp;</td>
                                <td><?php echo $p['id_survei']; ?></td>
                                <td><?php echo $p['nama_pertanyaan']; ?></td>
                                <td><?php echo $p['jenis_jawaban']; ?></td>
                                <td>
                                    <?php if ($p['jawaban_a'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_a'];
                                       } ?>
                                </td>
                                <td>
                                    <?php if ($p['jawaban_b'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_b'];
                                       } ?>
                                </td>
                                <td>
                                    <?php if ($p['jawaban_c'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_c'];
                                       } ?>
                                </td>
                                <td>
                                    <?php if ($p['jawaban_d'] == null){
                                           echo "-";
                                        }else{
                                           echo $p['jawaban_d'];
                                       } ?>
                                </td>
                                <td><?php echo $p['created_at']; ?></td> 
                                <td><?php echo $p['update_at']; ?></td> 
                                <!-- <td><a href=""></a></td>  -->
                            </tr>
                            <?php $i++; }?>
                        </tbody>
				</table>        
			</div>      

    </section>
   <!-- /.content -->
  </div>
<script src="<?php echo base_url('resources/js/jquery-1.11.2.min.js') ?>"></script>  
<script>
  $(document).ready( function () {
 		$('#myTable').DataTable({
        "scrollX": true
    });
  } );
</script>