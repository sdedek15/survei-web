 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div>';
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Pertanyaan</h3>
		<hr />   

		<?php echo form_open_multipart($action);?>
		
		<div class="form-group">
				<?php 
					// echo form_label('Id Survei');
					echo form_error('id_survei');
					echo form_input($id_survei);
				?>				
			</div>	
		<div class="form-group">
				<?php 
					
					echo form_label('Nama Survei');
					echo form_input($nama_survei);
					// echo form_input($nama_survei,$nama_survei);
				?>				
			</div>	
	    <div class="form-group">
				<?php 
					echo form_label('Nama Pertanyaan');
					echo form_error('nama_pertanyaan');
					echo form_input($nama_pertanyaan);
				?>				
			</div>
		<div class="form-group">
				<?php
					echo form_label('Jenis Jawaban');
					echo form_error('jenis_jawaban');
					$jawaban = array(
						'' => '',
						'Radio' => 'Radio',
						'Text' => 'Text',
                        'Checkbox' => 'Checkbox',
                        'Date' => 'Date',
					);
					echo form_dropdown($jenis_jawaban,$jawaban,$jenis_jawaban['value']);
				?>				
			</div>

        <?php
        if ($perta->jawaban_a !=null){
            echo '
                <div class="field_wrapper">
                    <div class="form-group">
                        '.form_input($jawaban_a).'
                        <a href="javascript:void(0);" class="btn btn-primary add_button" title="Add field"><i class = "fa fa-plus"></i></a>
                    </div>
                </div>
            ';
        }else{
            echo '
                <div class="field_wrapper">
                    <div class="form-group">
                        <label>Jawaban</label><br>
                        <input type="text" name="jawaban[]" value=""/>
                        <a href="javascript:void(0);" class="btn btn-primary add_button" title="Add field"><i class = "fa fa-plus"></i></a>
                    </div>
                </div>
            ';
        }
        if ($perta->jawaban_b != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_b).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_c != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_c).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_d != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_d).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_e != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_e).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_f != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_f).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_g != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_g).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_h != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_h).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_i != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_i).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_j != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_j).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_k != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_k).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_l != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_l).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_m != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_m).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_n != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_n).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        if ($perta->jawaban_o != null){
            echo '
                    <div class="form-group field_wrapperr">
                        '.form_input($jawaban_o).'
                       <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a>
                    </div>
            ';
        }else{
            echo '';
        }
        ?>
		
        <div class="form-group">
				<?php 
					echo form_error('wajib_isi');
					if ($wajib_isi['value']=='Ya'){
					echo form_checkbox('wajib_isi', 'Ya', true);
					}else{
					echo form_checkbox('wajib_isi', 'Ya');
					}
					echo form_label('Wajib Isi');
				?>	
				<br>
			</div>
	    <?php 
			echo form_input($id);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('Survei','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
	<br>
	<p>NB: Untuk Jenis Jawaban Text dan Date tidak perlu input jawaban, biarkan saja kosong !!</p>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 15; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var wrapperr = $('.field_wrapperr'); //Input field wrapper
    var fieldHTML = '<div class="form-group"><input type="text" name="jawaban[]" value=""/> <a href="javascript:void(0);" class="btn btn-danger remove_button"> <i class = "fa fa-remove"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

    
