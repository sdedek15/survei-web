 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div>';
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Survei</h3>
		<hr />	 
		<?php echo form_open_multipart($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('Nama Survei');
					echo form_error('nama_survei');
					echo form_textarea($nama_survei);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Tanggal Mulai');
					echo form_error('tgl_mulai');
					echo form_input($tgl_mulai);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Tanggal Selesai');
					echo form_error('tgl_selesai');
					echo form_input($tgl_selesai);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Jam Mulai');
					echo form_error('jam_mulai');
					echo form_input($jam_mulai);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Jam Selesai');
					echo form_error('jam_selesai');
					echo form_input($jam_selesai);
				?>				
			</div>	
	    <div class="form-group">
				<?php

					echo form_label('Status');
					echo form_error('status');
					$aktif = array(
						'Aktif' => 'Aktif',
						'Tidak Aktif' => 'Tidak Aktif',
					);
					echo form_dropdown($status,$aktif,$status['value']);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_error('login');
					if ($login['value']=='Ya'){
					echo form_checkbox('login', 'Ya', true);
					}else{
					echo form_checkbox('login', 'Ya');
					}
					echo form_label('Perlu Login');
				?>	
				<br>
			</div>	
		
	    <?php 
			echo form_input($id);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('Survei','Batal',array('class'=>'btn btn-flat btn-default')); 
		?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    
