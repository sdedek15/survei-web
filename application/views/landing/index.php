<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Halaman Daftar Survei</title>
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/logo.png'); ?>">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/logo.png'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-icons.css'); ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-kit.css?v=2.0.4');?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css'); ?>">
  <style type="text/css">
    .toast { opacity: 1 !important; }
  </style>  
</head>
<body class="landing-page">
  <nav class="navbar navbar-dark default-color justify-content-between bg-primary">
  <div class="container">
    <img src="<?php echo base_url('assets/img/logo.png'); ?>" height="40px">
    <b><a class="navbar-brand">APP Survei</a></b>
    <!-- <div class="container"> -->
        
      
      <!-- <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item"> -->
            <a class="btn btn-info btn-outline-white btn-md my-2 my-sm-0 ml-3 pull-right" href="<?= base_url()?>auth"> <i class = "fa fa-key"></i> Login Admin </a>
          <!-- </li>
        </ul>
      </div> -->
    </div>
  </nav>   
  <div class="main main-raised" style="margin-top: 0.5%;">
  
    <!-- <a href="<?= base_url()?>auth" class="btn btn-gray pull-right"><i class = "fa fa-key"></i> Login Admin</a> -->
    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Halaman Daftar Survei</h2>
            <h5 class="description">Silahkan Isi Survei di bawah ini :</h5> 
          </div>
        </div>

           <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Survei</h3><hr />	
            </div>
            <div class="table-responsive-sm" >
				      <table class="table">
                <thead>
                            <tr>
                                <th>No</th>
                                
                                <th>Aksi</th>
                                <th>Nama Survei</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Jam Mulai</th>
                                <th>Jam Selesai</th>
                                <!-- <th>Status</th> -->
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i=1;
                            foreach ($survei as $srv){
                                error_reporting(0);
                            ?>
                            <tr>
                               
                                <td><?php echo $i; ?></td>
                                <td>
                                <?php
                                date_default_timezone_set('Asia/Jakarta');
                                $tgl_skrg = date('Y-m-d');
                                $jam_skrg = date('H:i:s');
                                if ($srv->tgl_mulai <= $tgl_skrg && $tgl_skrg <= $srv->tgl_selesai && $jam_skrg >= $srv->jam_mulai && $jam_skrg < $srv->jam_selesai){
                                echo '<a href="'.base_url().'IsiSurvei/getPertanyaan/'.$srv->id.'" class="btn btn-info"><i class = "fa fa-send"></i></a>';
                                }else{
                                  echo '';
                                }
                                ?>
                                </td> 
                                <td><?php echo $srv->nama_survei; ?></td>
                                <td><?php echo $srv->tgl_mulai; ?></td>
                                <td><?php echo $srv->tgl_selesai; ?></td>
                                <td><?php echo $srv->jam_mulai; ?></td>
                                <td><?php echo $srv->jam_selesai; ?></td>
                                <!-- <td><?php echo $srv->status; ?></td> -->
                                <td><?php echo $srv->created_at; ?></td> 
                                
                            </tr>
                            <?php $i++; }?>
                        </tbody>
				</table>        
			</div>      

  </div>
  </div>
  <footer class="footer footer-default">
    <div class="container">
      <div class="copyright float-right"> &copy;
        <script>
        document.write(new Date().getFullYear())
        </script>, <a href="#">Dedek Setiawan S.Kom</a>. </div>
    </div>
  </footer>
  <script type="text/javascript" src="<?php echo base_url('assets/js/core/jquery.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/core/popper.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/core/bootstrap-material-design.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/nouislider.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/material-kit.js?v=2.0.4'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/vendor/toastr/toastr.min.js'); ?>"></script>

  <script type="text/javascript">
  </script>
</body>
</html>