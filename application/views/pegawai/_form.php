 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div>';
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Pegawai</h3>
		<hr />	 
		<?php echo form_open_multipart($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('NIP');
					echo form_error('nip');
					echo form_input($nip);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Nama Pegawai');
					echo form_error('nama');
					echo form_input($nama);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Email');
					echo form_error('email');
					echo form_input($email);
				?>				
			</div>
	    <div class="form-group">
				<?php

					echo form_label('Gender');
					echo form_error('gender');
					$laki = array(
						'Laki-laki' => 'Laki-laki',
						'Perempuan' => 'Perempuan',
					);
					echo form_dropdown($gender,$laki,$gender['value']);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Tempat Lahir');
					echo form_error('tempat_lahir');
					echo form_input($tempat_lahir);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Tanggal Lahir');
					echo form_error('tgl_lahir');
					echo form_input($tgl_lahir);
				?>				
			</div>	
		<div class="form-group">
				<?php 
					echo form_label('No Hape Pegawai');
					echo form_error('no_hape_pegawai');
					echo form_input($no_hape_pegawai);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Keluarga Dekat');
					echo form_error('keluarga_dekat');
					echo form_input($keluarga_dekat);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('No Hape Keluarga');
					echo form_error('no_hape_keluarga');
					echo form_input($no_hape_keluarga);
				?>				
			</div>	
		<div class="form-group">
				<?php 
					echo form_label('Jabatan');
					echo form_error('jabatan');
					echo form_input($jabatan);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Posisi');
					echo form_error('posisi');
					echo form_input($posisi);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Unit');
					echo form_error('unit');
					echo form_input($unit);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Direktorat');
					echo form_error('direktorat');
					echo form_input($direktorat);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Lokasi');
					echo form_error('lokasi');
					echo form_input($lokasi);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Pendidikan');
					echo form_error('pendidikan');
					echo form_input($pendidikan);
				?>				
			</div>
		<div class="form-group">
				<?php 
					echo form_label('Area');
					echo form_error('area');
					echo form_input($area);
				?>				
			</div>
		<div class="form-group">
				<?php

					echo form_label('Status Pegawai');
					echo form_error('status_pegawai');
					$aktif = array(
						'Aktif' => 'Aktif',
						'Tidak Aktif' => 'Tidak Aktif',
					);
					echo form_dropdown($status_pegawai,$aktif,$status_pegawai['value']);
				?>				
			</div>
	    <?php 
			echo form_input($id);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('Pegawai','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    
