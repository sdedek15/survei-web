<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Halaman Isi Survei</title>
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/logo.png'); ?>">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/logo.png'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-icons.css'); ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-kit.css?v=2.0.4');?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css'); ?>">
  <style type="text/css">
    .toast { opacity: 1 !important; }
  </style>  
</head>
<body class="landing-page sidebar-collapse">
   
  <div class="main main-raised" style="margin-top: 0.5%;">
    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Halaman Isi Survei</h2>
            <h5 class="description">Silahkan Lengkapi Pertanyaan Survei di bawah ini :</h5> 
          </div>
        </div>
        <?php 
          foreach ($survei as $srv => $s){
                  error_reporting(0);
          }
        ?>
        <!-- <h3 class="box-title hidden">Id Survei : <?php echo $s['id']; ?></h3> -->
        <h3 class="box-title">Judul Survei : <?php echo $s['nama_survei']; ?></h3>
                  <?php 
                      foreach ($pertanyaan as $p):
                              // error_reporting(0);
                  ?> 
            <form class="survei-form" method="POST">
              <div class="row">
                <div class="col-md-12">
                
                    <input type="hidden" name="id_survei" value="<?php echo $s['id']; ?>"  >
                    <input type="hidden" name="nama_survei" value="<?php echo $s['nama_survei']; ?>"  >
                    <input type="hidden" name="nip" value="<?php echo $nip; ?>"  >

                  
                  <div class="form-group">
                    <?php if ($p['wajib_isi'] == 'Ya') {
                      echo '<label class="info-title" name="nama_pertanyaan[]">'.$p['nama_pertanyaan'].'</label> <label style="color:red">*</label><br>';
                    }else{
                      echo '<label class="info-title" name="nama_pertanyaan[]">'.$p['nama_pertanyaan'].'</label><br>';
                    }
                    ?>
                    <input type="hidden" name="id_pertanyaan[]" value="<?php echo $p['id']; ?>"  >
                    <input type="hidden" name="nama_pertanyaan[]" value="<?php echo $p['nama_pertanyaan']; ?>"  >
                    <input type="hidden" name="jenis_jawaban[]" value="<?php echo $p['jenis_jawaban']; ?>"  >
                  
                    <?php 
                      if ($p['jenis_jawaban']=='Radio'){
                        echo '<div class="form-group">';
                        if ($p['jawaban_a']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_a'].'" required=""> <label>'.$p['jawaban_a'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_b']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_b'].'"> <label>'.$p['jawaban_b'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_c']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_c'].'"> <label>'.$p['jawaban_c'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_d']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_d'].'"> <label>'.$p['jawaban_d'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_e']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_e'].'"> <label>'.$p['jawaban_e'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_f']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_f'].'"> <label>'.$p['jawaban_f'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_g']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_g'].'"> <label>'.$p['jawaban_g'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_h']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_h'].'"> <label>'.$p['jawaban_h'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_i']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_i'].'"> <label>'.$p['jawaban_i'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_j']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_j'].'"> <label>'.$p['jawaban_j'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_k']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_k'].'"> <label>'.$p['jawaban_k'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_l']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_l'].'"> <label>'.$p['jawaban_l'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_m']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_m'].'"> <label>'.$p['jawaban_m'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_n']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_n'].'"> <label>'.$p['jawaban_n'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_o']!=null){
                        echo '<input type="radio" name="jawaban[]" value="'.$p['jawaban_o'].'"> <label>'.$p['jawaban_o'].'</label>';
                        }else{
                          echo '';
                        }
                        echo '</div>';

                      }else if ($p['jenis_jawaban']== 'Text'){
                        echo '<div class="form-group">';
                        echo '<input type="text" name="jawaban[]" class="form-control" required="">';
                        echo '</div>';

                      }else if ($p['jenis_jawaban']== 'Date') {
                        echo '<div class="form-group">';
                        echo '<input type="date" name="jawaban[]" class="form-control" required="">';
                        echo '</div>';

                      }else if ($p['jenis_jawaban']== 'Checkbox'){
                        echo '<div class="form-group">';
                        if ($p['jawaban_a']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_a'].'" required=""> <label>'.$p['jawaban_a'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_b']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_b'].'"> <label>'.$p['jawaban_b'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_c']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_c'].'"> <label>'.$p['jawaban_c'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_d']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_d'].'"> <label>'.$p['jawaban_d'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_e']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_e'].'"> <label>'.$p['jawaban_e'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_f']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_f'].'"> <label>'.$p['jawaban_f'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_g']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_g'].'"> <label>'.$p['jawaban_g'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_h']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_h'].'"> <label>'.$p['jawaban_h'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_i']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_i'].'"> <label>'.$p['jawaban_i'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_j']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_j'].'"> <label>'.$p['jawaban_j'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_k']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_k'].'"> <label>'.$p['jawaban_k'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_l']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_l'].'"> <label>'.$p['jawaban_l'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_m']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_m'].'"> <label>'.$p['jawaban_m'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_n']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_n'].'"> <label>'.$p['jawaban_n'].'</label><p>';
                        }else{
                          echo '';
                        }
                        if ($p['jawaban_o']!=null){
                        echo '<input type="checkbox" name="jawaban[]" value="'.$p['jawaban_o'].'"> <label>'.$p['jawaban_o'].'</label>';
                        }else{
                          echo '';
                        }
                        echo '</div>';
                      }else {
                        echo '----------';
                      }
                    ?>
                    </div>  
                    
                  </div>
                </div>
                </form>
                <?php
                  endforeach;
                ?> 
                <br>
              <div class="row">
                  <div class="col-md-4 ml-auto mr-auto text-center">
                    <button class="btn btn-info btn-raised tombol-simpan">Simpan</button>
                  </div>
              </div>   
              <!-- </form> -->
              
                 

  </div>
  </div>
  <footer class="footer footer-default">
    <div class="container">
      <div class="copyright float-right"> &copy;
        <script>
        document.write(new Date().getFullYear())
        </script>, <a href="#">Dedek Setiawan S.Kom</a>. </div>
    </div>
  </footer>
  <script type="text/javascript" src="<?php echo base_url('assets/js/core/jquery.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/core/popper.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/core/bootstrap-material-design.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/nouislider.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/material-kit.js?v=2.0.4'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/vendor/toastr/toastr.min.js'); ?>"></script>

  <script type="text/javascript">
	$(document).ready(function(){
		$(".tombol-simpan").click(function(){
			var data = $('.survei-form').serialize();
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url('SaveSurvei/save'); ?>",
				data: data,
				success: function() {
					// $('.tampildata').load("tampil.php");
          // var ok = confirm("Apakah anda yakin akan mengirim survei ini ?")
          // if(ok){
          $(location).attr('href','<?php echo base_url('Landing'); ?>')
          // }else{
            // document.write('');
          // }
				}
			});
		});
	});
	</script>
</body>
</html>