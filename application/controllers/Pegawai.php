<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Pegawai_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
		$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
						
			$this->data['title'] = 'Pegawai';
			$this->get_Meta();
			
			$this->data['_view']='pegawai/_list';
			$this->_render_page('layouts/main',$this->data);
		}
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Pegawai_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
			
			$row = $this->Pegawai_model->get_by_id($id);
			if ($row) {
				$this->data['id'] = $this->form_validation->set_value('id',$row->id);
				$this->data['nip'] = $this->form_validation->set_value('nip',$row->nip);
				$this->data['nama'] = $this->form_validation->set_value('nama',$row->nama);
				$this->data['email'] = $this->form_validation->set_value('email',$row->email);
				$this->data['gender'] = $this->form_validation->set_value('gender',$row->gender);
				$this->data['tempat_lahir'] = $this->form_validation->set_value('tempat_lahir',$row->tempat_lahir);
				$this->data['tgl_lahir'] = $this->form_validation->set_value('tgl_lahir',$row->tgl_lahir);
				$this->data['no_hape_pegawai'] = $this->form_validation->set_value('no_hape_pegawai',$row->no_hape_pegawai);
				$this->data['keluarga_dekat'] = $this->form_validation->set_value('keluarga_dekat',$row->keluarga_dekat);
				$this->data['no_hape_keluarga'] = $this->form_validation->set_value('no_hape_keluarga',$row->no_hape_keluarga);
				$this->data['jabatan'] = $this->form_validation->set_value('jabatan',$row->jabatan);
				$this->data['posisi'] = $this->form_validation->set_value('posisi',$row->posisi);
				$this->data['unit'] = $this->form_validation->set_value('unit',$row->unit);
				$this->data['direktorat'] = $this->form_validation->set_value('direktorat',$row->direktorat);
				$this->data['lokasi'] = $this->form_validation->set_value('lokasi',$row->lokasi);
				$this->data['pendidikan'] = $this->form_validation->set_value('pendidikan',$row->pendidikan);
				$this->data['area'] = $this->form_validation->set_value('area',$row->area);
				$this->data['status_pegawai'] = $this->form_validation->set_value('status_pegawai',$row->status_pegawai);
				$this->data['created_at'] = $this->form_validation->set_value('created_at',$row->created_at);
				$this->data['update_at'] = $this->form_validation->set_value('update_at',$row->update_at);
	    
				$this->data['title'] = 'Pegawai';
				$this->get_Meta();
				$this->data['_view'] = 'pegawai/_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('Pegawai'));
			}
		}
    }

    public function create() 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();			
			$this->data['user'] = $this->ion_auth->user()->row();
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$this->data['get_parent'] = $this->Pegawai_model->get_all();
			
			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('Pegawai/create_action');
		    $this->data['id'] = array(
				'name'			=> 'id',
				'type'			=> 'hidden',
				'value'			=> $this->form_validation->set_value('id'),
				'class'			=> 'form-control',
			);
		    $this->data['nip'] = array(
				'name'			=> 'nip',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nip'),
				'class'			=> 'form-control',
			);
		    $this->data['nama'] = array(
				'name'			=> 'nama',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama'),
				'class'			=> 'form-control',
			);
		    $this->data['email'] = array(
				'name'			=> 'email',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('email'),
				'class'			=> 'form-control',
			);
		    $this->data['gender'] = array(
				'name'			=> 'gender',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('gender'),
				'class'			=> 'form-control select2',
			);
		    $this->data['tempat_lahir'] = array(
				'name'			=> 'tempat_lahir',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('tempat_lahir'),
				'class'			=> 'form-control',
			);
		    $this->data['tgl_lahir'] = array(
				'name'			=> 'tgl_lahir',
				'type'			=> 'date',
				'value'			=> $this->form_validation->set_value('tgl_lahir'),
				'class'			=> 'form-control',
			);
		    $this->data['no_hape_pegawai'] = array(
				'name'			=> 'no_hape_pegawai',
				'type'			=> 'number',
				'value'			=> $this->form_validation->set_value('no_hape_pegawai'),
				'class'			=> 'form-control',
            );
            $this->data['keluarga_dekat'] = array(
				'name'			=> 'keluarga_dekat',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('keluarga_dekat'),
				'class'			=> 'form-control',
            );
            $this->data['no_hape_keluarga'] = array(
				'name'			=> 'no_hape_keluarga',
				'type'			=> 'number',
				'value'			=> $this->form_validation->set_value('no_hape_keluarga'),
				'class'			=> 'form-control',
            );
            $this->data['jabatan'] = array(
				'name'			=> 'jabatan',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('jabatan'),
				'class'			=> 'form-control',
            );
            $this->data['posisi'] = array(
				'name'			=> 'posisi',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('posisi'),
				'class'			=> 'form-control',
            );
            $this->data['unit'] = array(
				'name'			=> 'unit',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('unit'),
				'class'			=> 'form-control',
            );
            $this->data['direktorat'] = array(
				'name'			=> 'direktorat',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('direktorat'),
				'class'			=> 'form-control',
            );
            $this->data['lokasi'] = array(
				'name'			=> 'lokasi',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('lokasi'),
				'class'			=> 'form-control',
            );
            $this->data['pendidikan'] = array(
				'name'			=> 'pendidikan',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('pendidikan'),
				'class'			=> 'form-control',
            );
            $this->data['area'] = array(
				'name'			=> 'area',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('peareandidikan'),
				'class'			=> 'form-control',
            );
            $this->data['status_pegawai'] = array(
				'name'			=> 'status_pegawai',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('status_pegawai'),
				'class'			=> 'form-control select2',
			);
	
			$this->data['title'] = 'Pegawai';
			$this->get_Meta();
			$this->data['_view'] = 'pegawai/_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }
    
    public function create_action() 
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            

            $data = array(
			'nip' 			            => $this->input->post('nip',TRUE),
			'nama' 			            => $this->input->post('nama',TRUE),
			'email' 		            => $this->input->post('email',TRUE),
			'gender' 					=> $this->input->post('gender',TRUE),
			'tempat_lahir' 			    => $this->input->post('tempat_lahir',TRUE),
			'tgl_lahir' 			    => $this->input->post('tgl_lahir',TRUE),
			'no_hape_pegawai' 			=> $this->input->post('no_hape_pegawai',TRUE),
			'keluarga_dekat' 			=> $this->input->post('keluarga_dekat',TRUE),
			'no_hape_keluarga' 			=> $this->input->post('no_hape_keluarga',TRUE),
			'jabatan' 			        => $this->input->post('jabatan',TRUE),
			'posisi' 			        => $this->input->post('posisi',TRUE),
			'unit' 			            => $this->input->post('unit',TRUE),
			'direktorat' 			    => $this->input->post('direktorat',TRUE),
			'lokasi' 			        => $this->input->post('lokasi',TRUE),
			'pendidikan' 			    => $this->input->post('pendidikan',TRUE),
			'area' 			            => $this->input->post('area',TRUE),
			'status_pegawai' 			=> $this->input->post('status_pegawai',TRUE),
			'created_at' 			    => date("Y-m-d H:i:s"),
			);

            $this->Pegawai_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('Pegawai'));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
			$this->data['user'] = $this->ion_auth->user()->row();
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			
			
			// $this->data['get_parent'] = $this->Pegawai_model->get_all();
			
			$row = $this->Pegawai_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('Pegawai/update_action');
			    $this->data['id'] = array(
                    'name'			=> 'id',
                    'type'			=> 'hidden',
                    'value'			=> $this->form_validation->set_value('id', $row->id),
                    'class'			=> 'form-control',
                );
                $this->data['nip'] = array(
                    'name'			=> 'nip',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('nip', $row->nip),
                    'class'			=> 'form-control',
                );
                $this->data['nama'] = array(
                    'name'			=> 'nama',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('nama', $row->nama),
                    'class'			=> 'form-control',
                );
                $this->data['email'] = array(
                    'name'			=> 'email',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('email', $row->email),
                    'class'			=> 'form-control',
                );
                $this->data['gender'] = array(
                    'name'			=> 'gender',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('gender', $row->gender),
                    'class'			=> 'form-control select2',
                );
                $this->data['tempat_lahir'] = array(
                    'name'			=> 'tempat_lahir',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('tempat_lahir', $row->tempat_lahir),
                    'class'			=> 'form-control',
                );
                $this->data['tgl_lahir'] = array(
                    'name'			=> 'tgl_lahir',
                    'type'			=> 'date',
                    'value'			=> $this->form_validation->set_value('tgl_lahir', $row->tgl_lahir),
                    'class'			=> 'form-control',
                );
                $this->data['no_hape_pegawai'] = array(
                    'name'			=> 'no_hape_pegawai',
                    'type'			=> 'number',
                    'value'			=> $this->form_validation->set_value('no_hape_pegawai', $row->no_hape_pegawai),
                    'class'			=> 'form-control',
                );
                $this->data['keluarga_dekat'] = array(
                    'name'			=> 'keluarga_dekat',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('keluarga_dekat', $row->keluarga_dekat),
                    'class'			=> 'form-control',
                );
                $this->data['no_hape_keluarga'] = array(
                    'name'			=> 'no_hape_keluarga',
                    'type'			=> 'number',
                    'value'			=> $this->form_validation->set_value('no_hape_keluarga', $row->no_hape_keluarga),
                    'class'			=> 'form-control',
                );
                $this->data['jabatan'] = array(
                    'name'			=> 'jabatan',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jabatan', $row->jabatan),
                    'class'			=> 'form-control',
                );
                $this->data['posisi'] = array(
                    'name'			=> 'posisi',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('posisi', $row->posisi),
                    'class'			=> 'form-control',
                );
                $this->data['unit'] = array(
                    'name'			=> 'unit',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('unit', $row->unit),
                    'class'			=> 'form-control',
                );
                $this->data['direktorat'] = array(
                    'name'			=> 'direktorat',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('direktorat', $row->direktorat),
                    'class'			=> 'form-control',
                );
                $this->data['lokasi'] = array(
                    'name'			=> 'lokasi',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('lokasi', $row->lokasi),
                    'class'			=> 'form-control',
                );
                $this->data['pendidikan'] = array(
                    'name'			=> 'pendidikan',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('pendidikan', $row->pendidikan),
                    'class'			=> 'form-control',
                );
                $this->data['area'] = array(
                    'name'			=> 'area',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('area', $row->area),
                    'class'			=> 'form-control',
                );
                $this->data['status_pegawai'] = array(
                    'name'			=> 'status_pegawai',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('status_pegawai', $row->status_pegawai),
                    'class'			=> 'form-control',
                );
	   
				$this->data['title'] = 'Pegawai';
				$this->get_Meta();
				$this->data['_view'] = 'pegawai/_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('Pegawai'));
			}
		}
    }
    
    public function update_action() 
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            
           $data = array(
			'nip' 			            => $this->input->post('nip',TRUE),
			'nama' 			            => $this->input->post('nama',TRUE),
			'email' 		            => $this->input->post('email',TRUE),
			'gender' 					=> $this->input->post('gender',TRUE),
			'tempat_lahir' 			    => $this->input->post('tempat_lahir',TRUE),
			'tgl_lahir' 			    => $this->input->post('tgl_lahir',TRUE),
			'no_hape_pegawai' 			=> $this->input->post('no_hape_pegawai',TRUE),
			'keluarga_dekat' 			=> $this->input->post('keluarga_dekat',TRUE),
			'no_hape_keluarga' 			=> $this->input->post('no_hape_keluarga',TRUE),
			'jabatan' 			        => $this->input->post('jabatan',TRUE),
			'posisi' 			        => $this->input->post('posisi',TRUE),
			'unit' 			            => $this->input->post('unit',TRUE),
			'direktorat' 			    => $this->input->post('direktorat',TRUE),
			'lokasi' 			        => $this->input->post('lokasi',TRUE),
			'pendidikan' 			    => $this->input->post('pendidikan',TRUE),
			'area' 			            => $this->input->post('area',TRUE),
			'status_pegawai' 			=> $this->input->post('status_pegawai',TRUE),
			'update_at' 			    => date("Y-m-d H:i:s"),
			);

            $this->Pegawai_model->update($this->input->post('id', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('Pegawai'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pegawai_model->get_by_id($id);

        if ($row) {
            $this->Pegawai_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('Pegawai'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('Pegawai'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['web_name'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('gender', 'gender', 'trim|required');
	$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
	$this->form_validation->set_rules('tgl_lahir', 'tanggal lahir', 'trim|required');
	$this->form_validation->set_rules('no_hape_pegawai', 'no hape pegawai', 'trim|required');
	$this->form_validation->set_rules('keluarga_dekat', 'keluarga dekat', 'trim|required');
	$this->form_validation->set_rules('no_hape_keluarga', 'no hape keluarga', 'trim|required');
	$this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required');
	$this->form_validation->set_rules('posisi', 'posisi', 'trim|required');
	$this->form_validation->set_rules('unit', 'unit', 'trim|required');
	$this->form_validation->set_rules('direktorat', 'direktorat', 'trim|required');
	$this->form_validation->set_rules('lokasi', 'lokasi', 'trim|required');
	$this->form_validation->set_rules('pendidikan', 'pendidikan', 'trim|required');
	$this->form_validation->set_rules('area', 'area', 'trim|required');
	$this->form_validation->set_rules('status_pegawai', 'status pegawai', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pegawai.php */
/* Location: ./application/controllers/pegawai.php */
