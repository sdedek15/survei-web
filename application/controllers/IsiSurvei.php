<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class IsiSurvei extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Survei_model','Pertanyaan_model','Pegawai_model'));
        $this->load->library(array('form_validation'));
		$this->load->helper(array('url', 'html'));        
		$this->load->library('datatables');
    }

    // public function index($id=null)
    // {	
    //     // $data['survei'] = $this->Survei_model->get_all();
    //     $data['pertanyaan'] = $this->Pertanyaan_model->getList($id);
	// 	$data['title'] = 'Isi Survei';
	// 	$this->load->view("isisurvei/index", $data);
    // } 

    public function getPertanyaan($id=null)
    {	
        $survei = $this->Survei_model->getById($id);
        foreach ($survei as $srv){
            $login = $srv['login'];
        }

        if ($login == 'Ya'){
            // echo 'You Must Login First';
            $data['id'] = $id;
            $this->load->view("isisurvei/login", $data); 

        }else{
            $data['survei'] = $this->Survei_model->getById($id);
            $data['pertanyaan'] = $this->Pertanyaan_model->getList($id);
            $data['title'] = 'Isi Survei';
            $this->load->view("isisurvei/index", $data);
        }
    } 

    public function login(){
        $id = $this->input->post('id', TRUE);
        $nip = $this->input->post('nip', TRUE);

        $pegawai = $this->Pegawai_model->getById($nip);
        foreach ($pegawai as $pgw){
            $nipp = $pgw['nip'];
        }

        if ($pegawai){
            $data['nip'] = $this->input->post('nip', TRUE);
            // redirect(site_url('IsiSurvei/getPertanyaanLogin/'.$id));

            $data['survei'] = $this->Survei_model->getById($id);
            $data['pertanyaan'] = $this->Pertanyaan_model->getList($id);
            $data['title'] = 'Isi Survei';
            $this->load->view("isisurvei/index", $data);
        }else{
            // echo 'Nip anda salah atau tidak terdaftar di database';
            // echo '<br>';
            // echo 'Silahkan hubungi Admin!!';
            $this->load->view("isisurvei/notfound");
        }
    }

    public function getPertanyaanLogin($id=null)
    {	
        $data['nip'] = $this->input->post('nip',TRUE);

        $data['survei'] = $this->Survei_model->getById($id);
        $data['pertanyaan'] = $this->Pertanyaan_model->getList($id);
        $data['title'] = 'Isi Survei';
        $this->load->view("isisurvei/index", $data);
    } 

}