<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Survei extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Survei_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
		$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
						
			$this->data['title'] = 'Survei';
			$this->get_Meta();
			
			$this->data['_view']='survei/_list';
			$this->_render_page('layouts/main',$this->data);
		}
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Survei_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
			
			$row = $this->Survei_model->get_by_id($id);
			if ($row) {
				$this->data['id'] = $this->form_validation->set_value('id',$row->id);
				$this->data['nama_survei'] = $this->form_validation->set_value('nama_survei',$row->nama_survei);
				$this->data['tgl_mulai'] = $this->form_validation->set_value('tgl_mulai',$row->tgl_mulai);
				$this->data['tgl_selesai'] = $this->form_validation->set_value('tgl_selesai',$row->tgl_selesai);
				$this->data['status'] = $this->form_validation->set_value('status',$row->status);
				$this->data['created_at'] = $this->form_validation->set_value('created_at',$row->created_at);
				$this->data['update_at'] = $this->form_validation->set_value('update_at',$row->update_at);
	    
				$this->data['title'] = 'Survei';
				$this->get_Meta();
				$this->data['_view'] = 'survei/_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('Survei'));
			}
		}
    }

    public function create() 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();			
			$this->data['user'] = $this->ion_auth->user()->row();
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$this->data['get_parent'] = $this->Survei_model->get_all();
			
			$this->data['button'] = 'Simpan';
			$this->data['action'] = site_url('Survei/create_action');
		    $this->data['id'] = array(
				'name'			=> 'id',
				'type'			=> 'hidden',
				'value'			=> $this->form_validation->set_value('id'),
				'class'			=> 'form-control',
			);
		    $this->data['nama_survei'] = array(
				'name'			=> 'nama_survei',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama_survei'),
				'class'			=> 'form-control',
			);
		    $this->data['tgl_mulai'] = array(
				'name'			=> 'tgl_mulai',
				'type'			=> 'date',
				'value'			=> $this->form_validation->set_value('tgl_mulai'),
				'class'			=> 'form-control',
			);
		    $this->data['tgl_selesai'] = array(
				'name'			=> 'tgl_selesai',
				'type'			=> 'date',
				'value'			=> $this->form_validation->set_value('tgl_selesai'),
				'class'			=> 'form-control',
			);
			$this->data['jam_mulai'] = array(
				'name'			=> 'jam_mulai',
				'type'			=> 'time',
				'value'			=> $this->form_validation->set_value('jam_mulai'),
				'class'			=> 'form-control',
			);
			$this->data['jam_selesai'] = array(
				'name'			=> 'jam_selesai',
				'type'			=> 'time',
				'value'			=> $this->form_validation->set_value('jam_selesai'),
				'class'			=> 'form-control',
			);
		    $this->data['status'] = array(
				'name'			=> 'status',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('status'),
				'class'			=> 'form-control select2',
			);
			$this->data['login'] = array(
				'name'			=> 'login',
				'type'			=> 'checkbox',
				'value'			=> $this->form_validation->set_value('login'),
				'class'			=> 'form-control',
			);
	
			$this->data['title'] = 'Survei';
			$this->get_Meta();
			$this->data['_view'] = 'survei/_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }
    
    public function create_action() 
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            
			$id = $this->input->post('id',TRUE);
			
            $data = array(
			'nama_survei' 			    => $this->input->post('nama_survei',TRUE),
			'tgl_mulai' 			    => $this->input->post('tgl_mulai',TRUE),
			'tgl_selesai' 		        => $this->input->post('tgl_selesai',TRUE),
			'jam_mulai' 		        => $this->input->post('jam_mulai',TRUE),
			'jam_selesai' 		        => $this->input->post('jam_selesai',TRUE),
			'status' 					=> $this->input->post('status',TRUE),
			'login' 					=> $this->input->post('login',TRUE),
			'created_at' 			    => date("Y-m-d H:i:s"),
			);

			$this->data['message'] = 'Data berhasil ditambahkan';
            $this->Survei_model->insert($data);
            
            redirect(site_url('Survei'));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
			$this->data['user'] = $this->ion_auth->user()->row();
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			
			
			$row = $this->Survei_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('Survei/update_action');
			    $this->data['id'] = array(
                    'name'			=> 'id',
                    'type'			=> 'hidden',
                    'value'			=> $this->form_validation->set_value('id', $row->id),
                    'class'			=> 'form-control',
                );
                $this->data['nama_survei'] = array(
                    'name'			=> 'nama_survei',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('nama_survei', $row->nama_survei),
                    'class'			=> 'form-control',
                );
                $this->data['tgl_mulai'] = array(
                    'name'			=> 'tgl_mulai',
                    'type'			=> 'date',
                    'value'			=> $this->form_validation->set_value('tgl_mulai', $row->tgl_mulai),
                    'class'			=> 'form-control',
                );
                $this->data['tgl_selesai'] = array(
                    'name'			=> 'tgl_selesai',
                    'type'			=> 'date',
                    'value'			=> $this->form_validation->set_value('tgl_selesai', $row->tgl_selesai),
                    'class'			=> 'form-control',
				);
				$this->data['jam_mulai'] = array(
                    'name'			=> 'jam_mulai',
                    'type'			=> 'time',
                    'value'			=> $this->form_validation->set_value('jam_mulai', $row->jam_mulai),
                    'class'			=> 'form-control',
				);
				$this->data['jam_selesai'] = array(
                    'name'			=> 'jam_selesai',
                    'type'			=> 'time',
                    'value'			=> $this->form_validation->set_value('jam_selesai', $row->jam_selesai),
                    'class'			=> 'form-control',
				);
                $this->data['status'] = array(
                    'name'			=> 'status',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('status', $row->status),
                    'class'			=> 'form-control',
				);
				$this->data['login'] = array(
                    'name'			=> 'login',
                    'type'			=> 'checkbox',
					'value'			=> $this->form_validation->set_value('login', $row->login),
                    'class'			=> 'form-control',
                );
                
	   
				$this->data['title'] = 'Survei';
				$this->get_Meta();
				$this->data['_view'] = 'survei/_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('Survei'));
			}
		}
    }
    
    public function update_action() 
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            
           $data = array(
			'nama_survei' 			    => $this->input->post('nama_survei',TRUE),
			'tgl_mulai' 			    => $this->input->post('tgl_mulai',TRUE),
			'tgl_selesai' 		        => $this->input->post('tgl_selesai',TRUE),
			'jam_mulai' 		        => $this->input->post('jam_mulai',TRUE),
			'jam_selesai' 		        => $this->input->post('jam_selesai',TRUE),
			'status' 					=> $this->input->post('status',TRUE),
			'login' 					=> $this->input->post('login',TRUE),
			'update_at' 			    => date("Y-m-d H:i:s"),
			);

            $this->Survei_model->update($this->input->post('id', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('Survei'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Survei_model->get_by_id($id);

        if ($row) {
            $this->Survei_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('Survei'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('Survei'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['web_name'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('nama_survei', 'nama survei', 'trim|required');
	$this->form_validation->set_rules('tgl_mulai', 'tanggal mulai', 'trim|required');
	$this->form_validation->set_rules('tgl_selesai', 'tanggal selesai', 'trim|required');
	$this->form_validation->set_rules('jam_mulai', 'jam mulai', 'trim|required');
	$this->form_validation->set_rules('jam_selesai', 'jam selesai', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file survei.php */
/* Location: ./application/controllers/survei.php */
