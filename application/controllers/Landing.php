<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landing extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Survei_model'));
        $this->load->library(array('form_validation'));
		$this->load->helper(array('url', 'html'));        
		$this->load->library('datatables');
    }

    public function index()
    {	
        $data['survei'] = $this->Survei_model->get_all();
		$data['title'] = 'Survei';
		$this->load->view("landing/index", $data);
    } 

}