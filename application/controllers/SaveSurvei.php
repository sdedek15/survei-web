<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SaveSurvei extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Survei_model','Pertanyaan_model','SaveSurvei_model'));
        $this->load->library(array('form_validation'));
		$this->load->helper(array('url', 'html'));        
		$this->load->library('datatables');
    }

	public function save()
	{
		date_default_timezone_set('Asia/Jakarta');
		$pertanyaan = $this->Pertanyaan_model->getList($id);

		// $id_pertanyaan = $this->input->post('id_pertanyaan', TRUE);
		// $nama_pertanyaan = $this->input->post('nama_pertanyaan', TRUE);
		// $jenis_jawaban = $this->input->post('jenis_jawaban', TRUE);
		// $jawaban = $this->input->post('jawaban', TRUE);


		// $data = array();
		// foreach($id_pertanyaan as $key => $val)
		// {
		

			// if ($_POST['jenis_jawaban'][$key] == 'Checkbox'){
				
			// 	// $result_jawaban = implode(', ', $_POST['jawabancheckbox']);
			// 	$result_jawaban = implode(', ', $_POST['jawaban'][$key]); 

			// }elseif ($_POST['jenis_jawaban'][$key] == 'Text'){

			// 	$result_jawaban = $_POST['jawaban'][$key];

			// }elseif ($_POST['jenis_jawaban'][$key] == 'Radio'){
				
			// 	$result_jawaban = $_POST['jawaban'][$key];
			// }else{
			// 	$result_jawaban = '';
			// }

		// 	$data[] = array(
		// 		'nip_pegawai' 			    => $this->input->post('nip', TRUE),
        //         'id_survei' 			    => $this->input->post('id_survei'),
        //         'nama_survei' 			    => $this->input->post('nama_survei'),
		// 		'id_pertanyaan' 			=> $val,
		// 		'nama_pertanyaan' 			=> $nama_pertanyaan[$key],
		// 		'jenis_jawaban' 			=> $jenis_jawaban[$key],
		// 		'jawaban' 					=> $jawaban[$key],
		// 		'created_at' 			    => date("Y-m-d H:i:s"),
		// 	);

		// }

		for($i=0; $i < count($this->input->post('id_pertanyaan', TRUE)); $i++){

			if ($this->input->post('jenis_jawaban', TRUE)[$i] == 'Checkbox'){
				
				$jawabanr = implode(', ', $this->input->post('jawaban', TRUE)); 
			}else{
				$jawabanr = $this->input->post('jawaban', TRUE);
			}

            $data[] = array(
                'nip_pegawai' 			    => $this->input->post('nip', TRUE),
                'id_survei' 			    => $this->input->post('id_survei', TRUE),
                'nama_survei' 			    => $this->input->post('nama_survei', TRUE),
				'id_pertanyaan' 			=> $this->input->post('id_pertanyaan', TRUE)[$i],
				'nama_pertanyaan' 			=> $this->input->post('nama_pertanyaan', TRUE)[$i],
				'jenis_jawaban' 			=> $this->input->post('jenis_jawaban', TRUE)[$i],
				// 'jawaban' 					=> $jawabanr[$i],
				'jawaban' 					=> $this->input->post('jawaban', TRUE)[$i],
				'created_at' 			    => date("Y-m-d H:i:s")
			);
			$jawaban[] = $this->input->post('jawaban', TRUE)[$i];
		}       

		// $id_pertanyaan = $_POST['id_pertanyaan'];
		// $nama_pertanyaan = $_POST['nama_pertanyaan'];
		// $jenis_jawaban = $_POST['jenis_jawaban'];
		// $jawaban = $_POST['jawaban'];
		// // $jawabancheck = $_POST['jawabancheck'];


		// $data = array();
    
		// $index = 0; // Set index array awal dengan 0
		// foreach($id_pertanyaan as $dataid){ // Kita buat perulangan berdasarkan idpertanyaan sampai data terakhir
		// 	// if ($jenis_jawaban[$index] == 'Checkbox'){
		// 	// 	$jawaban = implode(', ', $_POST['jawaban[]']);
		// 	// }else{
		// 	// 	$jawaban = $_POST['jawaban'];
		// 	// }	
			
		// array_push($data, array(
		// 	'nip_pegawai' 			    => $this->input->post('nip', TRUE),
        //     'id_survei' 			    => $this->input->post('id_survei', TRUE),
        //     'nama_survei' 			    => $this->input->post('nama_survei', TRUE),
		// 	'id_pertanyaan' 			=> $dataid,
		// 	'nama_pertanyaan' 			=> $nama_pertanyaan[$index],
		// 	'jenis_jawaban' 			=> $jenis_jawaban[$index],
		// 	'jawaban' 					=> $jawaban[$index],
		// 	'created_at' 			    => date("Y-m-d H:i:s")
		// ));
		// // print_r($data);
		// $index++;
		
		// }
            
        // $this->SaveSurvei_model->insert($data);
        $this->db->insert_batch('hasil_survei',$data);
        $this->data['message'] = 'Data berhasil ditambahkan';
		redirect(site_url('Landing'));	
		
	}

	public function _rules() 
    {
	$this->form_validation->set_rules('jawabantext', 'Jawaban', 'trim|required');
	$this->form_validation->set_rules('jawabanradio', 'Jawaban', 'trim|required');
	$this->form_validation->set_rules('jawabancheckbox', 'Jawaban', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}