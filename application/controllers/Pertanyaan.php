<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pertanyaan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Survei_model','Pertanyaan_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
		$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
						
			$this->data['title'] = 'Pertanyaan';
			$this->get_Meta();
			
			$this->data['_view']='pertanyaan/_list2';
			$this->_render_page('layouts/main',$this->data);
		}
    } 

    public function getList($id=null)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{			
			// set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        

			//list the users
			$this->data['title'] = 'Pertanyaan';
			$this->data['usr'] = $this->ion_auth->user()->row();
			$this->data['user'] = $this->ion_auth->user()->row();
			$this->data['pertanyaan'] = $this->Pertanyaan_model->getList($id);



			$this->get_Meta();
			$this->data['_view'] = 'pertanyaan/_list';
			
			$this->_render_page('layouts/main', $this->data);	
		}
	}	
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Pertanyaan_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
			
			$row = $this->Pertanyaan_model->get_by_id($id);
			if ($row) {
				$this->data['id'] = $this->form_validation->set_value('id',$row->id);
				$this->data['id_survei'] = $this->form_validation->set_value('id_survei',$row->id_survei);
				$this->data['nama_pertanyaan'] = $this->form_validation->set_value('nama_pertanyaan',$row->nama_pertanyaan);
				$this->data['jenis_jawaban'] = $this->form_validation->set_value('jenis_jawaban',$row->jenis_jawaban);
				$this->data['jawaban_a'] = $this->form_validation->set_value('jawaban_a',$row->jawaban_a);
				$this->data['jawaban_b'] = $this->form_validation->set_value('jawaban_b',$row->jawaban_b);
				$this->data['jawaban_c'] = $this->form_validation->set_value('jawaban_c',$row->jawaban_c);
				$this->data['jawaban_d'] = $this->form_validation->set_value('jawaban_d',$row->jawaban_d);
				$this->data['created_at'] = $this->form_validation->set_value('created_at',$row->created_at);
				$this->data['update_at'] = $this->form_validation->set_value('update_at',$row->update_at);
	    
				$this->data['title'] = 'Pertanyaan';
				$this->get_Meta();
				$this->data['_view'] = 'Pertanyaan/_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('Pertanyaan'));
			}
		}
    }

    public function create($id=null) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();			
			$this->data['user'] = $this->ion_auth->user()->row();
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->data['pertanyaan'] = $this->Pertanyaan_model->getList($id);
			// $this->data['get_survei'] = $this->Survei_model->getById($id);
			$survei = $this->Survei_model->get_by_id($id);
			// foreach ($survei as $sur) {
			// 		$nama_survei = $sur['nama_survei'];
			// }
			if($survei){
			$this->data['button'] = 'Simpan';
			$this->data['action'] = site_url('Pertanyaan/create_action');
		    $this->data['id'] = array(
				'name'			=> 'id',
				'type'			=> 'hidden',
				'value'			=> $this->form_validation->set_value('id'),
				'class'			=> 'form-control',
            );
            $this->data['id_survei'] = array(
				'name'			=> 'id_survei',
				'type'			=> 'hidden',
				'value'			=> $this->form_validation->set_value('id_survei',$id),
				'class'			=> 'form-control',
			);
			$this->data['nama_survei'] = array(
				'name'			=> 'nama_survei',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama_survei',$survei->nama_survei),
				'class'			=> 'form-control',
				'readonly'		=> true,
			);
		    $this->data['nama_pertanyaan'] = array(
				'name'			=> 'nama_pertanyaan',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama_pertanyaan'),
				'class'			=> 'form-control',
			);
		    $this->data['jenis_jawaban'] = array(
				'name'			=> 'jenis_jawaban',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('jenis_jawaban'),
				'class'			=> 'form-control select2',
			);
		    $this->data['jawaban'] = array(
				'name'			=> 'jawaban',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('jawaban'),
				'class'			=> 'form-control',
			);
			$this->data['wajib_isi'] = array(
				'name'			=> 'wajib_isi',
				'type'			=> 'checkbox',
				'value'			=> $this->form_validation->set_value('wajib_isi'),
				'class'			=> 'form-control',
			);
	
			$this->data['title'] = 'Pertanyaan';
			$this->get_Meta();
			$this->data['_view'] = 'pertanyaan/_form';
			$this->_render_page('layouts/main',$this->data);
			}
		}
    }
    
    public function create_action() 
    {
		error_reporting(0);
        date_default_timezone_set('Asia/Jakarta');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            
        
            $id = $this->input->post('id_survei');
            $jawaban = $this->input->post('jawaban');
            
            
            foreach($jawaban as $key)
            {
                $jawaban_a = $this->input->post('jawaban')[0];
                $jawaban_b = $this->input->post('jawaban')[1];
                $jawaban_c = $this->input->post('jawaban')[2];
                $jawaban_d = $this->input->post('jawaban')[3];
                $jawaban_e = $this->input->post('jawaban')[4];
                $jawaban_f = $this->input->post('jawaban')[5];
                $jawaban_g = $this->input->post('jawaban')[6];
                $jawaban_h = $this->input->post('jawaban')[7];
                $jawaban_i = $this->input->post('jawaban')[8];
                $jawaban_j = $this->input->post('jawaban')[9];
                $jawaban_k = $this->input->post('jawaban')[10];
                $jawaban_l = $this->input->post('jawaban')[11];
                $jawaban_m = $this->input->post('jawaban')[12];
                $jawaban_n = $this->input->post('jawaban')[13];
                $jawaban_o = $this->input->post('jawaban')[14];
            }

          
                
                $data = array(
                'id_survei' 			    => $id,
                'nama_pertanyaan' 			=> $this->input->post('nama_pertanyaan',TRUE),
                'jenis_jawaban' 			=> $this->input->post('jenis_jawaban',TRUE),
                'jawaban_a' 		        => $jawaban_a,
                'jawaban_b' 		        => $jawaban_b,
                'jawaban_c' 		        => $jawaban_c,
                'jawaban_d' 		        => $jawaban_d,
                'jawaban_e' 		        => $jawaban_e,
                'jawaban_f' 		        => $jawaban_f,
                'jawaban_g' 		        => $jawaban_g,
                'jawaban_h' 		        => $jawaban_h,
                'jawaban_i' 		        => $jawaban_i,
                'jawaban_j' 		        => $jawaban_j,
                'jawaban_k' 		        => $jawaban_k,
                'jawaban_l' 		        => $jawaban_l,
                'jawaban_m' 		        => $jawaban_m,
                'jawaban_n' 		        => $jawaban_n,
                'jawaban_o' 		        => $jawaban_o,
                'wajib_isi' 		        => $this->input->post('wajib_isi', TRUE),
                'created_at' 			    => date("Y-m-d H:i:s"),
                );
                
            
            $this->Pertanyaan_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('Pertanyaan/create/'.$id));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();
			$this->data['user'] = $this->ion_auth->user()->row();
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			
			
			$row = $this->Pertanyaan_model->get_by_id($id);

			if ($row) {

				$survei= $this->Survei_model->get_by_id($row->id_survei);
				$this->data['perta'] = $this->Pertanyaan_model->get_by_id($id);
				$this->data['pertanyaan'] = $this->Pertanyaan_model->getList($id);
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('Pertanyaan/update_action');
			    $this->data['id'] = array(
                    'name'			=> 'id',
                    'type'			=> 'hidden',
                    'value'			=> $this->form_validation->set_value('id', $row->id),
                    'class'			=> 'form-control',
				);
				$this->data['id_survei'] = array(
					'name'			=> 'id_survei',
					'type'			=> 'hidden',
					'value'			=> $this->form_validation->set_value('id_survei',$row->id_survei),
					'class'			=> 'form-control',
				);
				$this->data['nama_survei'] = array(
					'name'			=> 'nama_survei',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nama_survei', $survei->nama_survei),
					'class'			=> 'form-control',
					'readonly'		=> true,
				);
                $this->data['nama_pertanyaan'] = array(
                    'name'			=> 'nama_pertanyaan',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('nama_pertanyaan', $row->nama_pertanyaan),
                    'class'			=> 'form-control',
                );
                $this->data['jenis_jawaban'] = array(
                    'name'			=> 'jenis_jawaban',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jenis_jawaban', $row->jenis_jawaban),
                    'class'			=> 'form-control select2',
                );
                $this->data['jawaban_a'] = array(
                    'name'			=> 'jawaban',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_a', $row->jawaban_a),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_b'] = array(
                    'name'			=> 'jawaban_b',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_b', $row->jawaban_b),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_c'] = array(
                    'name'			=> 'jawaban_c',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_c', $row->jawaban_c),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_d'] = array(
                    'name'			=> 'jawaban_d',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_d', $row->jawaban_d),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_e'] = array(
                    'name'			=> 'jawaban_e',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_e', $row->jawaban_e),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_f'] = array(
                    'name'			=> 'jawaban_f',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_f', $row->jawaban_f),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_g'] = array(
                    'name'			=> 'jawaban_g',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_g', $row->jawaban_g),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_h'] = array(
                    'name'			=> 'jawaban_h',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_h', $row->jawaban_h),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_i'] = array(
                    'name'			=> 'jawaban_i',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_i', $row->jawaban_i),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_j'] = array(
                    'name'			=> 'jawaban_j',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_j', $row->jawaban_j),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_k'] = array(
                    'name'			=> 'jawaban_k',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_k', $row->jawaban_k),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_l'] = array(
                    'name'			=> 'jawaban_l',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_l', $row->jawaban_l),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_m'] = array(
                    'name'			=> 'jawaban_m',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_m', $row->jawaban_m),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_n'] = array(
                    'name'			=> 'jawaban_n',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_n', $row->jawaban_n),
                    'class'			=> 'form-group',
				);
				$this->data['jawaban_o'] = array(
                    'name'			=> 'jawaban_o',
                    'type'			=> 'text',
                    'value'			=> $this->form_validation->set_value('jawaban_o', $row->jawaban_o),
                    'class'			=> 'form-group',
                );
                $this->data['wajib_isi'] = array(
                    'name'			=> 'wajib_isi',
                    'type'			=> 'checkbox',
                    'value'			=> $this->form_validation->set_value('wajib_isi', $row->wajib_isi),
                    'class'			=> 'form-control',
                );
	   
				$this->data['title'] = 'Pertanyaan';
				$this->get_Meta();
				$this->data['_view'] = 'pertanyaan/edit_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('Pertanyaan/create/'.$id_survei));
			}
		}
    }
    
    public function update_action() 
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

			$id_survei = $this->input->post('id_survei');
			$jawaban = $this->input->post('jawaban');
            
            
            foreach($jawaban as $key)
            {
             	$jawaban_a = $this->input->post('jawaban')[0];
                $jawaban_b = $this->input->post('jawaban')[1];
                $jawaban_c = $this->input->post('jawaban')[2];
                $jawaban_d = $this->input->post('jawaban')[3];
                $jawaban_e = $this->input->post('jawaban')[4];
                $jawaban_f = $this->input->post('jawaban')[5];
                $jawaban_g = $this->input->post('jawaban')[6];
                $jawaban_h = $this->input->post('jawaban')[7];
                $jawaban_i = $this->input->post('jawaban')[8];
                $jawaban_j = $this->input->post('jawaban')[9];
                $jawaban_k = $this->input->post('jawaban')[10];
                $jawaban_l = $this->input->post('jawaban')[11];
                $jawaban_m = $this->input->post('jawaban')[12];
                $jawaban_n = $this->input->post('jawaban')[13];
                $jawaban_o = $this->input->post('jawaban')[14];
            }
            
           $data = array(
				'id_survei' 			    => $id_survei,
                'nama_pertanyaan' 			=> $this->input->post('nama_pertanyaan',TRUE),
                'jenis_jawaban' 			=> $this->input->post('jenis_jawaban',TRUE),
                'jawaban_a' 		        => $jawaban_a,
                'jawaban_b' 		        => $jawaban_b,
                'jawaban_c' 		        => $jawaban_c,
                'jawaban_d' 		        => $jawaban_d,
                'jawaban_e' 		        => $jawaban_e,
                'jawaban_f' 		        => $jawaban_f,
                'jawaban_g' 		        => $jawaban_g,
                'jawaban_h' 		        => $jawaban_h,
                'jawaban_i' 		        => $jawaban_i,
                'jawaban_j' 		        => $jawaban_j,
                'jawaban_k' 		        => $jawaban_k,
                'jawaban_l' 		        => $jawaban_l,
                'jawaban_m' 		        => $jawaban_m,
                'jawaban_n' 		        => $jawaban_n,
                'jawaban_o' 		        => $jawaban_o,
				'wajib_isi' 		        => $this->input->post('wajib_isi', TRUE),
                'update_at' 			    => date("Y-m-d H:i:s"),
			);

            $this->Pertanyaan_model->update($this->input->post('id', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('Pertanyaan/create/'.$id_survei));
        }
    }
    
    public function delete($id) 
    {
		$row = $this->Pertanyaan_model->get_by_id($id);
		
		$ids = $this->input->post('id_survei');

        if ($row) {
            $this->Pertanyaan_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('Survei'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('Survei'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['web_name'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('id_survei', 'id survei', 'trim|required');
	$this->form_validation->set_rules('nama_pertanyaan', 'nama pertanyaan', 'trim|required');
	$this->form_validation->set_rules('jenis_jawaban', 'jenis_jawaban', 'trim|required');
	// $this->form_validation->set_rules('jawaban', 'jawaban', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pertanyaan.php */
/* Location: ./application/controllers/Pertanyaan.php */
