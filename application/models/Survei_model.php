<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Survei_model extends CI_Model
{

    public $table = 'survei';
    public $id = 'id';
    public $order = 'DESC';	

    function __construct()
    {
        parent::__construct();
    }
		
    // datatables
    function json() {
        $this->datatables->select('id,nama_survei,tgl_mulai,tgl_selesai,jam_mulai,jam_selesai,status,created_at,update_at');
        $this->datatables->from('survei');
        $this->datatables->add_column('action', anchor(site_url('Survei/update/$1'),'<i class = "fa fa-edit"></i>', array('class'=>'btn btn-flat btn-warning'))." ".anchor(site_url('Survei/delete/$1'),'<i class = "fa fa-trash"></i>', array('class'=>'btn btn-flat btn-danger'),'onclick="javascript: return confirm(\'Are You Sure ?\')"'), 'id');
        $this->datatables->add_column('pertanyaan', anchor(site_url('Pertanyaan/create/$1'),'<i class = "fa fa-plus"></i> Create Pertanyaan', array('class'=>'btn btn-flat btn-info')), 'id');
        return $this->datatables->generate();
    }

    function getById($id=null) {
        $this->db->order_by('id','DESC');
        return $this->db->get_where('survei', ['id' => $id])->result_array();
    }
	
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('nama_survei', $q);
		$this->db->or_like('tgl_mulai', $q);
		$this->db->or_like('tgl_selesai', $q);
		$this->db->or_like('status', $q);
		$this->db->or_like('created_at', $q);
		$this->db->or_like('update_at', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('nama_survei', $q);
		$this->db->or_like('tgl_mulai', $q);
		$this->db->or_like('tgl_selesai', $q);
		$this->db->or_like('status', $q);
		$this->db->or_like('created_at', $q);
		$this->db->or_like('update_at', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Karyawan_model.php */
/* Location: ./application/models/Karyawan_model.php */
