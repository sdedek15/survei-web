<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class HasilSurvei_model extends CI_Model
{

    public $table = 'hasil_survei';
    public $id = 'id';
    public $order = 'DESC';	

    function __construct()
    {
        parent::__construct();
    }
		
    // datatables
    function json() {
        $this->datatables->select('hasil_survei.id,hasil_survei.nama_survei,hasil_survei.created_at,pegawai.nama as nama, pegawai.nip as nip');
        $this->datatables->from('hasil_survei');
        //add this line for join
        $this->datatables->join('pegawai', 'pegawai.nip = hasil_survei.nip_pegawai');
        // $this->datatables->join('survei', 'hasil_survei.id_survei = survei.id');
        $this->datatables->add_column('action', anchor(site_url('HasilSurvei/read/$1'),'<i class = "fa fa-eye"></i>', array('class'=>'btn btn-flat btn-info')), 'id');
        return $this->datatables->generate();
    }

    function getById($id=null) {
        $this->db->order_by('id','DESC');
        return $this->db->get_where('hasil_survei', ['id' => $id])->result_array();
    }
	
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('nip_pegawai', $q);
		$this->db->or_like('id_survei', $q);
		$this->db->or_like('nama_survei', $q);
		$this->db->or_like('id_pertanyaan', $q);
		$this->db->or_like('nama_pertanyaan', $q);
		$this->db->or_like('jenis_jawaban', $q);
		$this->db->or_like('jawaban', $q);
		$this->db->or_like('created_at', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('nip_pegawai', $q);
		$this->db->or_like('id_survei', $q);
		$this->db->or_like('nama_survei', $q);
		$this->db->or_like('id_pertanyaan', $q);
		$this->db->or_like('nama_pertanyaan', $q);
		$this->db->or_like('jenis_jawaban', $q);
		$this->db->or_like('jawaban', $q);
		$this->db->or_like('created_at', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Karyawan_model.php */
/* Location: ./application/models/Karyawan_model.php */
