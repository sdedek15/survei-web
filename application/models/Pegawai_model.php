<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{

    public $table = 'pegawai';
    public $id = 'id';
    public $order = 'DESC';	

    function __construct()
    {
        parent::__construct();
    }
		
    // datatables
    function json() {
        $this->datatables->select('id,nip,nama,email,gender,tempat_lahir,tgl_lahir,no_hape_pegawai,keluarga_dekat,no_hape_keluarga,jabatan,posisi,unit,direktorat,lokasi,pendidikan,area,status_pegawai,created_at,update_at');
        $this->datatables->from('pegawai');
        //add this line for join
        //$this->datatables->join('table2', 'menu.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('Pegawai/read/$1'),'<i class = "fa fa-eye"></i>', array('class'=>'btn btn-flat btn-info'))." ".anchor(site_url('Pegawai/update/$1'),'<i class = "fa fa-edit"></i>', array('class'=>'btn btn-flat btn-warning'))." ".anchor(site_url('Pegawai/delete/$1'),'<i class = "fa fa-trash"></i>', array('class'=>'btn btn-flat btn-danger'),'onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    function getById($nip=null) {
        $this->db->order_by('nip','DESC');
        return $this->db->get_where('pegawai', ['nip' => $nip])->result_array();
    }
	
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('nip', $q);
		$this->db->or_like('nama', $q);
		$this->db->or_like('email', $q);
		$this->db->or_like('gender', $q);
		$this->db->or_like('tempat_lahir', $q);
		$this->db->or_like('tgl_lahir', $q);
		$this->db->or_like('no_hape_keluarga', $q);
		$this->db->or_like('keluarga_dekat', $q);
		$this->db->or_like('no_hape_keluarga', $q);
		$this->db->or_like('jabatan', $q);
		$this->db->or_like('posisi', $q);
		$this->db->or_like('unit', $q);
		$this->db->or_like('direktorat', $q);
		$this->db->or_like('lokasi', $q);
		$this->db->or_like('pendidikan', $q);
		$this->db->or_like('area', $q);
		$this->db->or_like('status_pegawai', $q);
		$this->db->or_like('created_at', $q);
		$this->db->or_like('update_at', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('nip', $q);
		$this->db->or_like('nama', $q);
		$this->db->or_like('email', $q);
		$this->db->or_like('gender', $q);
		$this->db->or_like('tempat_lahir', $q);
		$this->db->or_like('tgl_lahir', $q);
		$this->db->or_like('no_hape_keluarga', $q);
		$this->db->or_like('keluarga_dekat', $q);
		$this->db->or_like('no_hape_keluarga', $q);
		$this->db->or_like('jabatan', $q);
		$this->db->or_like('posisi', $q);
		$this->db->or_like('unit', $q);
		$this->db->or_like('direktorat', $q);
		$this->db->or_like('lokasi', $q);
		$this->db->or_like('pendidikan', $q);
		$this->db->or_like('area', $q);
		$this->db->or_like('status_pegawai', $q);
		$this->db->or_like('created_at', $q);
		$this->db->or_like('update_at', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Karyawan_model.php */
/* Location: ./application/models/Karyawan_model.php */
