-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2020 at 04:23 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `survei-fix`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'Grup1', 'grup 1');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_survei`
--

CREATE TABLE `hasil_survei` (
  `id` int(11) NOT NULL,
  `nip_pegawai` varchar(100) DEFAULT NULL,
  `id_survei` int(11) NOT NULL,
  `nama_survei` varchar(250) NOT NULL,
  `id_pertanyaan` int(11) NOT NULL,
  `nama_pertanyaan` varchar(250) NOT NULL,
  `jenis_jawaban` varchar(250) NOT NULL,
  `jawaban` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hasil_survei`
--

INSERT INTO `hasil_survei` (`id`, `nip_pegawai`, `id_survei`, `nama_survei`, `id_pertanyaan`, `nama_pertanyaan`, `jenis_jawaban`, `jawaban`, `created_at`) VALUES
(988, '', 12, 'Tessssss', 52, 'Jenis Kelamin ?', 'Radio', 'Laki-laki', '2020-08-30 17:14:06'),
(989, '', 12, 'Tessssss', 53, 'Siapa Anda ?', 'Text', 'Dedek', '2020-08-30 17:14:06'),
(990, '', 12, 'Tessssss', 55, 'Alasan bekerja ?', 'Radio', 'Gaji', '2020-08-30 17:14:06'),
(991, '', 12, 'Tessssss', 60, 'Apakah sudah selesai ?', 'Radio', 'Sudah', '2020-08-30 17:14:06'),
(992, '', 12, 'Tessssss', 61, 'Siapa nama Ayah anda ?', 'Text', 'Suef', '2020-08-30 17:14:06'),
(993, '', 12, 'Tessssss', 62, 'Siapa nama Ibu anda ?', 'Text', 'Sipolan', '2020-08-30 17:14:06'),
(994, '', 12, 'Tessssss', 64, 'Tes 15 Jawaban', 'Radio', '15', '2020-08-30 17:14:06'),
(995, '', 12, 'Tessssss', 70, 'Tanggal Lahir ?', 'Date', '1995-07-08', '2020-08-30 17:14:06');

-- --------------------------------------------------------

--
-- Table structure for table `identitas_web`
--

CREATE TABLE `identitas_web` (
  `id_identitas` int(11) NOT NULL,
  `nama_web` varchar(255) NOT NULL,
  `meta_deskripsi` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `copyright` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `identitas_web`
--

INSERT INTO `identitas_web` (`id_identitas`, `nama_web`, `meta_deskripsi`, `meta_keyword`, `copyright`, `logo`) VALUES
(1, 'Admin Survei Panel', 'Admin Survei Panel Aplikasi', 'Admin Survei Panel Aplikasi', 'Dedek Setiawan', './uploads/7ee1640e4dada063e2736d98efa2ff18.png');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `parent_menu` int(11) DEFAULT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `controller_link` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `urut_menu` int(11) NOT NULL,
  `menu_grup_user` varchar(30) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_menu`, `nama_menu`, `controller_link`, `icon`, `slug`, `urut_menu`, `menu_grup_user`, `is_active`) VALUES
(1, 0, 'Data Master', '', 'fa-list', '', 1, '1', 1),
(2, 0, 'Survei', 'Survei', 'fa-folder-open', 'Survei', 2, '1', 1),
(3, 1, 'Pegawai', 'Pegawai', 'fa-user', 'Pegawai', 2, '1', 1),
(4, 0, 'Hasil Survei', 'HasilSurvei', 'fa-print', 'HasilSurvei', 2, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_hape_pegawai` varchar(255) DEFAULT NULL,
  `keluarga_dekat` varchar(255) DEFAULT NULL,
  `no_hape_keluarga` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `direktorat` varchar(255) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `pendidikan` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `status_pegawai` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `nama`, `email`, `gender`, `tempat_lahir`, `tgl_lahir`, `no_hape_pegawai`, `keluarga_dekat`, `no_hape_keluarga`, `jabatan`, `posisi`, `unit`, `direktorat`, `lokasi`, `pendidikan`, `area`, `status_pegawai`, `created_at`, `update_at`) VALUES
(12, '123456789011', 'Aleena', 'aleena@gmail.com', 'Perempuan', 'Stabat', '2020-01-09', '8211111111091', 'ayah', '82111111107', 'Jabatan', 'Posisi', 'Unit', 'Direktorat', 'Lokasi', 'Pendidikan', 'Area', 'Aktif', '2020-07-26 09:08:41', '2020-08-27 18:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id` int(11) NOT NULL,
  `id_survei` int(11) DEFAULT NULL,
  `nama_pertanyaan` varchar(250) NOT NULL,
  `jenis_jawaban` varchar(250) NOT NULL,
  `jawaban_a` varchar(250) DEFAULT NULL,
  `jawaban_b` varchar(250) DEFAULT NULL,
  `jawaban_c` varchar(250) DEFAULT NULL,
  `jawaban_d` varchar(250) DEFAULT NULL,
  `jawaban_e` varchar(250) DEFAULT NULL,
  `jawaban_f` varchar(250) DEFAULT NULL,
  `jawaban_g` varchar(250) DEFAULT NULL,
  `jawaban_h` varchar(250) DEFAULT NULL,
  `jawaban_i` varchar(250) DEFAULT NULL,
  `jawaban_j` varchar(250) DEFAULT NULL,
  `jawaban_k` varchar(250) DEFAULT NULL,
  `jawaban_l` varchar(250) DEFAULT NULL,
  `jawaban_m` varchar(250) DEFAULT NULL,
  `jawaban_n` varchar(250) DEFAULT NULL,
  `jawaban_o` varchar(250) DEFAULT NULL,
  `wajib_isi` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `id_survei`, `nama_pertanyaan`, `jenis_jawaban`, `jawaban_a`, `jawaban_b`, `jawaban_c`, `jawaban_d`, `jawaban_e`, `jawaban_f`, `jawaban_g`, `jawaban_h`, `jawaban_i`, `jawaban_j`, `jawaban_k`, `jawaban_l`, `jawaban_m`, `jawaban_n`, `jawaban_o`, `wajib_isi`, `created_at`, `update_at`) VALUES
(32, 2, 'Siapa Anda ?', 'Radio', 'Sipolan', 'Suef', 'Dedek', 'Tampan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-26 20:01:30', NULL),
(33, 2, 'Siapa Anda ?', 'Checkbox', 'Saya Dedek', 'Saya Sipolan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-26 20:01:53', NULL),
(34, 2, 'Jenis Kelamin', 'Radio', 'Laki-laki', 'Perempuan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-26 20:04:54', NULL),
(35, 3, 'Jelaskan siapa anda ?', 'Radio', 'Suef', 'Sipolan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-26 20:20:36', NULL),
(36, 3, 'Siapa Anda ?', 'Checkbox', 'Suef', 'Sipolan', 'Dedek', 'Tampan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-26 20:23:51', NULL),
(41, 3, 'Pertanyaan', 'Radio', 'Jawaban 1', 'Jawaban 2', 'Jawaban 3', 'Jawaban 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-28 17:21:38', NULL),
(42, 3, 'Jelaskan Tentang Diri Anda ?', 'Text', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-28 17:24:09', NULL),
(43, 3, 'Siapa Anda ?', 'Radio', 'Saya Sipolan', 'Saya Dedek', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-30 16:27:48', NULL),
(44, 4, 'Siapa Anda ?', 'Radio', 'Saya Dedek', 'Saya Sipolan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-30 16:36:49', '2020-08-01 17:10:45'),
(50, 4, 'Siapa Saya', 'Checkbox', 'Suef', 'Sipolan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-07-30 17:24:33', NULL),
(52, 12, 'Jenis Kelamin ?', 'Radio', 'Laki-laki', 'Perempuan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-08-02 18:02:45', NULL),
(53, 12, 'Siapa Anda ?', 'Text', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-08-02 18:40:43', NULL),
(55, 12, 'Alasan bekerja ?', 'Radio', 'Gaji', 'Uang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2020-08-02 18:55:12', NULL),
(57, 13, 'Apakah tubuh anda fit ?', 'Radio', 'Ya', 'Tidak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya', '2020-08-05 16:18:14', NULL),
(58, 4, 'Pertanyaan Text', 'Text', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya', '2020-08-05 19:41:52', NULL),
(59, 4, 'Jenis Kelamin', 'Radio', 'Laki-laki', 'Perempuan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya', '2020-08-08 03:41:38', NULL),
(60, 12, 'Apakah sudah selesai ?', 'Radio', 'Sudah', 'Belum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya', '2020-08-18 17:48:48', NULL),
(61, 12, 'Siapa nama Ayah anda ?', 'Text', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya', '2020-08-18 17:49:15', NULL),
(62, 12, 'Siapa nama Ibu anda ?', 'Text', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-18 17:49:59', NULL),
(64, 12, 'Tes 15 Jawaban', 'Radio', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', 'Ya', '2020-08-30 16:26:34', NULL),
(70, 12, 'Tanggal Lahir ?', 'Date', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya', '2020-08-30 17:05:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `survei`
--

CREATE TABLE `survei` (
  `id` int(11) NOT NULL,
  `nama_survei` varchar(250) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `status` varchar(100) NOT NULL,
  `login` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `survei`
--

INSERT INTO `survei` (`id`, `nama_survei`, `tgl_mulai`, `tgl_selesai`, `jam_mulai`, `jam_selesai`, `status`, `login`, `created_at`, `update_at`) VALUES
(2, 'Survei 23', '2020-07-29', '2020-07-31', '00:00:00', '00:00:00', 'Aktif', NULL, '2020-07-26 13:59:11', '2020-07-26 13:59:44'),
(3, 'Survei atas nama saya dedek setiawan untuk kamu siapa saja', '2020-07-26', '2020-07-28', '00:00:00', '00:00:00', 'Aktif', NULL, '2020-07-26 15:37:28', NULL),
(4, 'Survei Berikutnya', '2020-07-31', '2020-10-31', '00:00:00', '23:59:00', 'Aktif', NULL, '2020-07-30 16:36:11', '2020-08-21 16:02:21'),
(12, 'Tessssss', '2020-08-03', '2020-10-31', '00:00:00', '23:59:00', 'Aktif', NULL, '2020-08-02 21:05:41', '2020-08-18 18:21:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$mZPaYks57MijwQIbpiXn0e5ugdDn8QTDaY0.jay3axX/dJGhcv7sG', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1598804372, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(7, 1, 1),
(8, 1, 2),
(9, 1, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_survei`
--
ALTER TABLE `hasil_survei`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `identitas_web`
--
ALTER TABLE `identitas_web`
  ADD PRIMARY KEY (`id_identitas`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survei`
--
ALTER TABLE `survei`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hasil_survei`
--
ALTER TABLE `hasil_survei`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=996;

--
-- AUTO_INCREMENT for table `identitas_web`
--
ALTER TABLE `identitas_web`
  MODIFY `id_identitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `survei`
--
ALTER TABLE `survei`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
